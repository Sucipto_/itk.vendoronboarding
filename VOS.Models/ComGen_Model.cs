﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VOS.Models
{
    public class ComGen_Model
    {
        public string Variable { get; set; }
        public string ValueDB { get; set; }
        public string ValueUI { get; set; }
        public string Description { get; set; }
    }
}
