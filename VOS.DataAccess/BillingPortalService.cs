﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VOS.Models;

namespace VOS.Services
{
    public class BillingPortalService
    {
        #region Create
        public string AddBilling(string ConnectionString, dbBilling_Model model)
        {
            var query = @"INSERT INTO [dbo].[dbBilling]
            ([VendID]
            ,[TicketID]
            ,[NoInvoice]
            ,[InvoiceDate]
            ,[Amount]
            ,[Currency]
            ,[PONumberITK]
            ,[BankID]
            ,[Status])
            OUTPUT INSERTED.BillID
            VALUES
            (@VendID
            ,@TicketID
            ,@NoInvoice
            ,@InvoiceDate
            ,@Amount
            ,@Currency
            ,@PONumberITK
            ,@BankID
            ,@Status)";

            var parameters = new DynamicParameters();
            //parameters.Add("BillID", model.BillID, DbType.String);
            parameters.Add("VendID", model.VendID, DbType.String);
            parameters.Add("TicketID", model.TicketID, DbType.String);
            parameters.Add("NoInvoice", model.NoInvoice, DbType.String);
            parameters.Add("InvoiceDate", model.InvoiceDate, DbType.DateTime);
            parameters.Add("Amount", model.Amount, DbType.Int32);
            parameters.Add("Currency", model.Currency, DbType.String);
            parameters.Add("PONumberITK", model.PONumberITK, DbType.String);
            //parameters.Add("AttachBilling", model.AttachBilling, DbType.String);
            parameters.Add("BankID", model.BankID, DbType.String);
            parameters.Add("Status", model.Status, DbType.String);
            //parameters.Add("StatusDate", model.StatusDate, DbType.DateTime);

            return new SqlConnection(ConnectionString).ExecuteScalar<string>(query, parameters);
        }
        #endregion

        #region Read

        //Populate Billing Portal for Vendor
        public List<dbBilling_Model> GetAllBillingPortalByVendID(string ConnectionString, string VendID)
        {
            var query = @"SELECT *
                        FROM [dbo].[dbBilling]
                        WHERE VendID = '" + VendID + "'";

            return new SqlConnection(ConnectionString).Query<dbBilling_Model>(query).ToList();
        }

        //Populate All Biling Portal for Admin Intikom
        public List<dbBilling_Model> GetAllBillingPortal(string ConnectionString, string VendID)
        {
            var query = @"SELECT bi.*, ba.Nama, ba.CabangBank, ba.NoRek 
                        FROM dbBilling bi
                        LEFT JOIN dbBank ba ON bi.BankID = ba.BankID
                        WHERE bi.VendID = '" + VendID + "'";

            return new SqlConnection(ConnectionString).Query<dbBilling_Model>(query).ToList();
        }

        public int GetIDBillingPlusOne(string ConnectionString)
        {
            var query = @"SELECT IDENT_CURRENT('dbBilling')+1";

            return new SqlConnection(ConnectionString).QueryFirstOrDefault<int>(query);
        }

        public List<dbKeuangan_Model> GetAllKeuanganIDbyVendID(string ConnectionString, string VendID)
        {
            var query = @"SELECT * FROM dbKeuangan
                        WHERE VendID = '" + VendID + "'";

            return new SqlConnection(ConnectionString).Query<dbKeuangan_Model>(query).ToList();
        }

        public List<dbBank_Model> GetDistinctBankNamebyKeuanganID(string ConnectionString, string KeuanganID)
        {
            var query = @"SELECT DISTINCT(Nama) FROM dbBank
                        WHERE KeuanganID = '" + KeuanganID + "'";

            return new SqlConnection(ConnectionString).Query<dbBank_Model>(query).ToList();
        }

        public List<dbBank_Model> GetDistinctCabangbyKeuanganID(string ConnectionString, string KeuanganID, string NamaBank)
        {
            var query = @"SELECT DISTINCT(CabangBank) FROM dbBank
                        WHERE KeuanganID = '" + KeuanganID + "'" + " AND Nama='" + NamaBank + "'";

            return new SqlConnection(ConnectionString).Query<dbBank_Model>(query).ToList();
        }

        public List<dbBank_Model> GetNoRekeningbyKNC(string ConnectionString, string KeuanganID, string NamaBank, string CabangBank)
        {
            var query = @"SELECT DISTINCT(NoRek) FROM dbBank
                        WHERE KeuanganID = '" + KeuanganID + "'" + " AND Nama='" + NamaBank + "'" + " AND CabangBank='" + CabangBank + "'";

            return new SqlConnection(ConnectionString).Query<dbBank_Model>(query).ToList();
        }

        public string GetBankIDbyNCR(string ConnectionString, string NamaBank, string CabangBank, string NoRekening)
        {
            var query = @"SELECT BankID from dbBank
                        WHERE Nama = '" + NamaBank + "' AND CabangBank = '" + CabangBank + "' AND NoRek = '" + NoRekening +"'";

            return new SqlConnection(ConnectionString).QueryFirstOrDefault<string>(query);
        }

        public List<dbBank_Model> GetBankInformationbyBankID(string ConnectionString, string BankID)
        {
            var query = @"SELECT * FROM dbBank
                        WHERE BankID = '" + BankID + "'";

            return new SqlConnection(ConnectionString).Query<dbBank_Model>(query).ToList();
        }

        #endregion

        #region Update
        public int UpdateBillingPortal(string ConnectionString, string VendID, dbBilling_Model model)
        {
            var query = @"UPDATE [dbo].[dbBilling]
                SET [Status] = @Status
                    ,[StatusDate] = @StatusDate
                WHERE VendID = '" + VendID + "'";

            var parameters = new DynamicParameters();
            parameters.Add("Status", model.Status, DbType.String);
            parameters.Add("StatusDate", model.StatusDate, DbType.DateTime);

            return new SqlConnection(ConnectionString).Execute(query, parameters);
        }

        public int UpdateAttachBilling(string ConnectionString, dbBilling_Model model)
        {
            var query = @"UPDATE [dbo].[dbBilling]
                SET [AttachBilling] = @AttachBilling
                    WHERE BillID = '" + model.BillID + "'";

            var parameters = new DynamicParameters();
            parameters.Add("AttachBilling", model.AttachBilling, DbType.String);

            return new SqlConnection(ConnectionString).Execute(query, parameters);
        }
        #endregion

        #region Delete

        #endregion
    }
}
