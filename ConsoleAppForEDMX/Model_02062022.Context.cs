﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ConsoleAppForEDMX
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class Models : DbContext
    {
        public Models()
            : base("name=Models")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<dbBank> dbBanks { get; set; }
        public virtual DbSet<dbCity> dbCities { get; set; }
        public virtual DbSet<dbCountry> dbCountries { get; set; }
        public virtual DbSet<dbFormatEdu> dbFormatEdus { get; set; }
        public virtual DbSet<dbKeuangan> dbKeuangans { get; set; }
        public virtual DbSet<dbLegal> dbLegals { get; set; }
        public virtual DbSet<dbLicense> dbLicenses { get; set; }
        public virtual DbSet<dbModal> dbModals { get; set; }
        public virtual DbSet<dbPajak> dbPajaks { get; set; }
        public virtual DbSet<dbPelatihan> dbPelatihans { get; set; }
        public virtual DbSet<dbPengalamanKerja> dbPengalamanKerjas { get; set; }
        public virtual DbSet<dbPenguru> dbPengurus { get; set; }
        public virtual DbSet<dbProdUnggul> dbProdUngguls { get; set; }
        public virtual DbSet<dbProvince> dbProvinces { get; set; }
        public virtual DbSet<dbSale> dbSales { get; set; }
        public virtual DbSet<dbSertifikat> dbSertifikats { get; set; }
        public virtual DbSet<dbSPT> dbSPTs { get; set; }
        public virtual DbSet<dbVendCV> dbVendCVs { get; set; }
        public virtual DbSet<dbVendorAkun> dbVendorAkuns { get; set; }
        public virtual DbSet<dbVendorNote> dbVendorNotes { get; set; }
        public virtual DbSet<sysdiagram> sysdiagrams { get; set; }
    }
}
