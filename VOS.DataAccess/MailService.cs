﻿using MailKit.Net.Smtp;
using MailKit.Security;
using Microsoft.Extensions.Options;
using MimeKit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VOS.Models;

namespace VOS.Services
{
    public class MailService
    {
        public void SendVerificationEmailEnglish(string EmailTo, string FirstPassword, string CompanyName, string EmailFrom, string PasswordFrom, string SMTP, int smtpPort)
        {
            //try
            //{
                string FilePath = Directory.GetCurrentDirectory() + "\\wwwroot\\mail-message-english.html";
                StreamReader str = new StreamReader(FilePath);
                string MailText = str.ReadToEnd();
                str.Close();
                MailText = MailText.Replace("[username]", CompanyName).Replace("[email]", EmailTo).Replace("[password]", FirstPassword);
                var email = new MimeMessage();
                email.Sender = MailboxAddress.Parse(EmailFrom);
                email.To.Add(MailboxAddress.Parse(EmailTo));
                email.Subject = $"PT Intikom Berlian Mustika - Verify Your Email";
                var builder = new BodyBuilder();
                builder.HtmlBody = MailText;
                email.Body = builder.ToMessageBody();
                using var smtp = new SmtpClient();
                smtp.Connect(SMTP, smtpPort, SecureSocketOptions.StartTls);
                smtp.Authenticate(EmailFrom, PasswordFrom);
                smtp.Send(email);
                smtp.Disconnect(true);

            //    return true;
            //}
            //catch
            //{
            //    return false;
            //}
        }
        public void SendVerificationEmailIndonesia(string EmailTo, string FirstPassword, string CompanyName, string EmailFrom, string PasswordFrom, string SMTP, int smtpPort)
        {
            //try
            //{
                string FilePath = Directory.GetCurrentDirectory() + "\\wwwroot\\mail-message-indonesia.html";
                StreamReader str = new StreamReader(FilePath);
                string MailText = str.ReadToEnd();
                str.Close();
                MailText = MailText.Replace("[username]", CompanyName).Replace("[email]", EmailTo).Replace("[password]", FirstPassword);
                var email = new MimeMessage();
                email.Sender = MailboxAddress.Parse(EmailFrom);
                email.To.Add(MailboxAddress.Parse(EmailTo));
                email.Subject = $"PT Intikom Berlian Mustika - Informasi Akun Anda";
                var builder = new BodyBuilder();
                builder.HtmlBody = MailText;
                email.Body = builder.ToMessageBody();
                using var smtp = new SmtpClient();
                smtp.Connect(SMTP, smtpPort, SecureSocketOptions.StartTls);
                smtp.Authenticate(EmailFrom, PasswordFrom);
                smtp.Send(email);
                smtp.Disconnect(true);

            //    return true;
            //}
            //catch
            //{
            //    return false;
            //}
        }
    }
}
