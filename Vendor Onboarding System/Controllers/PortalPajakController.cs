﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Newtonsoft.Json;
using Vendor_Onboarding_System.Localize;
using VOS.Models;

namespace Vendor_Onboarding_System.Controllers
{
    public class PortalPajakController : Controller
    {
        #region Session
        private string ConnectionString
        {
            get
            {
                try
                {
                    return JsonConvert.DeserializeObject<string>(HttpContext.Session.GetString("ConnectionString"));
                }
                catch
                {
                    return null;
                }
            }
        }

        private dbVendorAkun_Model CurrLoginUser
        {
            get
            {
                try
                {
                    return JsonConvert.DeserializeObject<dbVendorAkun_Model>(HttpContext.Session.GetString("CurrLoginUser"));
                }
                catch
                {
                    return null;
                }
            }
        }
        #endregion

        private readonly IStringLocalizer<Resource> _stringLocalizer;

        private readonly ILogger<HomeController> _logger;

        private readonly IConfiguration _config;

        public PortalPajakController(ILogger<HomeController> logger, IConfiguration config, IStringLocalizer<Resource> stringLocalizer)
        {
            _logger = logger;
            _config = config;
            _stringLocalizer = stringLocalizer;
        }

        public IActionResult Index()
        {
            ViewBag.Local = _stringLocalizer;
            if (CurrLoginUser == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                ViewBag.Login = true;
                ViewBag.Category = CurrLoginUser.Category;
            }

            return View();
        }
    }
}
