﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using VOS.Services;

namespace Vendor_Onboarding_System.Controllers
{
    public class ComGenController : Controller
    {
        private string ConnectionString
        {
            get
            {
                try
                {
                    return JsonConvert.DeserializeObject<string>(HttpContext.Session.GetString("ConnectionString"));
                }
                catch
                {
                    return null;
                }
            }
        }
        ComGenService comGen = new();
        public IActionResult Index()
        {
            return View();
        }
        public JsonResult GetComGenByVar(string vari)
        {
            return Json(comGen.GetComGenByVariable(ConnectionString, vari));
        }
        public JsonResult GetComGenByVarLikeEnd(string vari)
        {
            return Json(comGen.GetComGenByVariableLikePercent(ConnectionString, vari));
        }
    }
}
