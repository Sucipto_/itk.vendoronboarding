﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Vendor_Onboarding_System
{
    public class Encryptor
    {
        //private string strEncKey;
        private static byte[] encKey;
        private static byte[] encIV;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="TextToEncrypt"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        public static string Encrypt(string TextToEncrypt)
        {
            try
            {
                byte[] byteText = Encoding.ASCII.GetBytes(TextToEncrypt);
                byte[] byteKey = null;

                byteKey = Encoding.ASCII.GetBytes("3ncr1nt10nk3yf0r4pl!k@s1");

                return EncryptByte(byteText, byteKey);
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="EncryptedText"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        public static string Decrypt(string EncryptedText)
        {
            try
            {
                // Convert CipherText From Base64String to Byte()
                byte[] byteText = Convert.FromBase64String(EncryptedText);

                // Convert Key From Base64String to Byte()
                byte[] byteKey = Encoding.ASCII.GetBytes("3ncr1nt10nk3yf0r4pl!k@s1");

                return DecryptByte(byteText, byteKey);
            }
            catch (Exception)
            {
                return null;
            }
        }

        private static string EncryptByte(byte[] BytesData, byte[] Key)
        {
            MemoryStream memStreamEncryptedData;
            CryptoStream encStream;
            // Dim encProvider As Encrypt
            ICryptoTransform encICrypto = null;
            byte[] IV;
            try
            {
                memStreamEncryptedData = new MemoryStream();
                IV = Encoding.ASCII.GetBytes("3ncr1t0rk0d38305");

                encICrypto = RijndaelEncryptor(ref Key, ref IV);
                if (encICrypto != null)
                {
                    encStream = new CryptoStream(memStreamEncryptedData, encICrypto, CryptoStreamMode.Write);
                    encStream.Write(BytesData, 0, BytesData.Length);
                    encStream.FlushFinalBlock();
                    encStream.Close();
                }

                return Convert.ToBase64String(memStreamEncryptedData.ToArray());
            }
            catch (Exception ex)
            {
                // Return Nothing
                throw ex;
            }
            finally
            {
                memStreamEncryptedData = null;
                encStream = null;
                encICrypto = null;
            }
        }

        private static string DecryptByte(byte[] BytesData, byte[] Key)
        {
            MemoryStream memStreamEncryptedData;
            CryptoStream encStream;
            ICryptoTransform encICrypto = null;
            byte[] IV;
            try
            {
                memStreamEncryptedData = new MemoryStream();
                IV = Encoding.ASCII.GetBytes("3ncr1t0rk0d38305");

                encICrypto = RijndaelDescryptor(ref Key, ref IV);
                if (encICrypto != null)
                {
                    encStream = new CryptoStream(memStreamEncryptedData, encICrypto, CryptoStreamMode.Write);
                    encStream.Write(BytesData, 0, BytesData.Length);

                    encStream.FlushFinalBlock();
                    encStream.Close();
                }

                return Encoding.ASCII.GetString(memStreamEncryptedData.ToArray());
            }
            catch (Exception)
            {
                return null;
            }
            finally
            {
                memStreamEncryptedData = null;
                encStream = null;
                encICrypto = null;
            }
        }

        private static ICryptoTransform RijndaelEncryptor(ref byte[] bytesKey, ref byte[] byteIV)
        {
            RijndaelManaged DES = new RijndaelManaged();
            try
            {
                DES.Mode = CipherMode.CBC;

                if (bytesKey != null)
                    // DES.KeySize = bytesKey.Length - 1
                    DES.Key = bytesKey;
                encKey = DES.Key;

                if (byteIV != null)
                    // DES.BlockSize = byteIV.Length - 1
                    DES.IV = byteIV;
                encIV = DES.IV;

                return DES.CreateEncryptor();
            }
            catch (Exception)
            {
                return null;
            }
        }

        private static ICryptoTransform RijndaelDescryptor(ref byte[] bytesKey, ref byte[] byteIV)
        {
            RijndaelManaged DES = new RijndaelManaged();
            try
            {
                DES.Mode = CipherMode.CBC;
                if (byteIV == null)
                {
                    DES.GenerateIV();
                    byteIV = DES.IV;
                }
                return DES.CreateDecryptor(bytesKey, byteIV);
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}