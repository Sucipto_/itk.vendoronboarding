﻿using Dapper;
using System.Data.SqlClient;
using VOS.Models;

namespace VOS.Services
{
    public class CountryDetailService
    {
        #region Create

        #endregion

        #region Read
        public List<dbCountry_Model> GetAllCountry(string ConnectionString)
        {
            var query = @"SELECT * FROM [dbo].[dbCountry]";
            return new SqlConnection(ConnectionString).Query<dbCountry_Model>(query).ToList();
        }

        public List<dbCountry_Model> GetAllCurrency(string ConnectionString)
        {
            var query = @"SELECT name, currency FROM [dbo].[dbCountry]";
            return new SqlConnection(ConnectionString).Query<dbCountry_Model>(query).ToList();
        }

        public List<dbProvince_Model> GetAllStateByCountryID(string ConnectionString, int CountryID)
        {
            var query = @"SELECT * FROM [dbo].[dbProvince] WHERE country_id='" + CountryID + "'";
            return new SqlConnection(ConnectionString).Query<dbProvince_Model>(query).ToList();
        }

        public List<dbCity_Model> GetAllCitiesByStateID(string ConnectionString, int StateID)
        {
            var query = @"SELECT * FROM [dbo].[dbCity] WHERE state_id='" + StateID + "'";
            return new SqlConnection(ConnectionString).Query<dbCity_Model>(query).ToList();
        }
        #endregion

        #region Update

        #endregion

        #region Delete

        #endregion
    }
}
