﻿using Microsoft.SharePoint.Client;
using System;
using System.IO;
using System.Net;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace Example_CSOM_NET_Standar
{
    class Program
    {
        static void Test(string[] args)
        {
            try
            {
                #region URL 
                WebRequest webRequest;
                string strAccessTokenUrl = "https://accounts.accesscontrol.windows.net/{0}/tokens/OAuth/2 ";

                string tenantID = "fa53a526-b95e-432f-b6f5-45b7048bc217"; //can be identifiled from Azure Active Directory --> Properties
                string resourceID = "00000003-0000-0ff1-ce00-000000000000"; //it will be constant
                string accessToken = string.Empty;
                string strAppID = "20c3e9f9-c256-4b5b-bce0-fe04872311b7"; // pass the client id which is created in Step 1, https://<sitename>.sharepoint.com/_layouts/15/appregnew.aspx  
                string strAppSecret = "oXp8Q~6rJ-4pZ4DlHn0pv8lmnsO_SznAGyEOIbHU"; // pass the client secret code which is created in step 1 
                string strOrgDomain = "ptintikomberlianmustika.sharepoint.com";

                #endregion
                #region Get Access Token using TenantID and App secret ID & Password 
                // URL Format  
                //https://accounts.accesscontrol.windows.net/tenant_ID/tokens/OAuth/2  

                strAccessTokenUrl = string.Format(strAccessTokenUrl, tenantID);
                webRequest = WebRequest.Create(strAccessTokenUrl);
                webRequest.ContentType = "application/x-www-form-urlencoded";
                webRequest.Method = "POST";


                var postData = "grant_type=client_credentials";
                postData += "&client_id=" + strAppID + "@" + tenantID;
                postData += "&client_secret=" + strAppSecret;
                postData += "&resource=" + resourceID + "/" + strOrgDomain + "@" + tenantID;
                var data = Encoding.ASCII.GetBytes(postData);

                using (var stream = webRequest.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }
                var response = (HttpWebResponse)webRequest.GetResponse();

                var responseString = new StreamReader(response.GetResponseStream()).ReadToEnd();

                string[] stArrResponse = responseString.Split(',');

                //get the access token and expiry time ,etc 

                foreach (var stValues in stArrResponse)
                {

                    if (stValues.StartsWith("\"access_token\":"))
                    {
                        accessToken = stValues.Substring(16);
                        //Console.WriteLine(" Result => " + accessToken); 
                        accessToken = accessToken.Substring(0, accessToken.Length - 2);
                        // Console.WriteLine(" Result => " + accessToken); 
                    }
                }

                Console.WriteLine("Access Token --> {0}", accessToken);
                using (System.Net.WebClient client = new System.Net.WebClient())
                {
                    client.Headers.Add("X-FORMS_BASED_AUTH_ACCEPTED", "f");
                    client.Headers.Add("Authorization", "Bearer " + accessToken);
                    //string webUrl = "https://ptintikomberlianmustika.sharepoint.com/sites/SASDeveloperTeam/";
                    string webUrl = "https://ptintikomberlianmustika.sharepoint.com/sites/SASDeveloperTeam";
                    //string documentLibName = "Shared%20Documents/test";
                    string documentLibName = "VOB/Billing";
                    string fileName = "makan.jpg";

                    //link download nya
                    Uri endpointUri = new Uri(webUrl + "/_api/web/GetFolderByServerRelativeUrl('" + documentLibName + "')/Files('" + fileName + "')/$value");

                    //https://ptintikomberlianmustika.sharepoint.com/sites/SASDeveloperTeam/_api/web/GetFolderByServerRelativeUrl('VOB/Billing')/Files('makan.jpg')/$value

                    byte[] filedata = client.DownloadData(endpointUri);
                    FileStream outputStream = new FileStream(@"C:\" + fileName, FileMode.OpenOrCreate | FileMode.Append, FileAccess.Write, FileShare.None);
                    outputStream.Write(filedata, 0, filedata.Length);
                    outputStream.Flush(true);
                    outputStream.Close();
                }
                #endregion
                Console.ReadLine();
            }
            catch (Exception ex)
            {
                Console.WriteLine(" Error occured " + ex.Message);
                Console.ReadLine();
            }
        }
        public static async Task FixDownload(string[] args)
        {
            try
            {
                Uri site = new Uri("https://ptintikomberlianmustika.sharepoint.com/sites/SASDeveloperTeam");
                string user = "mbahariawan@intikom.co.id";
                SecureString password = new SecureString();
                foreach (char c in "Intikom2018") password.AppendChar(c);

                // Note: The PnP Sites Core AuthenticationManager class also supports this
                var accessToken = new AuthenticationManager().GetAccessToken(site, user, password);
                using (System.Net.WebClient client = new System.Net.WebClient())
                {
                    client.Headers.Add("X-FORMS_BASED_AUTH_ACCEPTED", "f");
                    client.Headers.Add("Authorization", "Bearer " + accessToken);
                    //string webUrl = "https://ptintikomberlianmustika.sharepoint.com/sites/SASDeveloperTeam/";
                    string webUrl = "https://ptintikomberlianmustika.sharepoint.com/sites/SASDeveloperTeam";
                    //string documentLibName = "Shared%20Documents/test";
                    string documentLibName = "VOB/Billing";
                    string fileName = "makan.jpg";

                    //link download nya
                    Uri endpointUri = new Uri(webUrl + "/_api/web/GetFolderByServerRelativeUrl('" + documentLibName + "')/Files('" + fileName + "')/$value");

                    //https://ptintikomberlianmustika.sharepoint.com/sites/SASDeveloperTeam/_api/web/GetFolderByServerRelativeUrl('VOB/Billing')/Files('makan.jpg')/$value

                    byte[] filedata = client.DownloadData(endpointUri);
                    FileStream outputStream = new FileStream(@"E:\Temp\" + fileName, FileMode.OpenOrCreate | FileMode.Append, FileAccess.Write, FileShare.None);
                    outputStream.Write(filedata, 0, filedata.Length);
                    outputStream.Flush(true);
                    outputStream.Close();
                }
            }
            catch (Exception ex)
            {
                var text = ex.Message;
            }
        }

        public static async Task FixUpload(string[] args)
        {
            try
            {
                Uri site = new Uri("https://ptintikomberlianmustika.sharepoint.com/sites/SASDeveloperTeam");
                string user = "mbahariawan@intikom.co.id";
                SecureString password = new SecureString();
                foreach (char c in "Intikom2018") password.AppendChar(c);

                // Note: The PnP Sites Core AuthenticationManager class also supports this
                using (var authenticationManager = new AuthenticationManager())
                using (var context = authenticationManager.GetContext(site, user, password))
                {
                    context.Load(context.Web, p => p.Title);
                    await context.ExecuteQueryAsync();
                    Console.WriteLine($"Title: {context.Web.Title}");

                    //coba upload file
                    Web web = context.Web;
                    context.Load(web);
                    context.ExecuteQuery();
                    FileCreationInformation newFile = new FileCreationInformation();
                    newFile.Content = System.IO.File.ReadAllBytes("E:\\makan.jpg");
                    newFile.Url = @"makan.jpg";
                    List byTitle = context.Web.Lists.GetByTitle("VOB");
                    Folder folder = byTitle.RootFolder.Folders.GetByUrl("Billing");
                    context.Load(folder);
                    context.ExecuteQuery();
                    Microsoft.SharePoint.Client.File uploadFile = folder.Files.Add(newFile);
                    uploadFile.CheckIn("checkin", CheckinType.MajorCheckIn);
                    context.Load(byTitle);
                    context.Load(uploadFile);
                    context.ExecuteQuery();
                    Console.WriteLine("done");

                    //List list = context.Web.Lists.GetByTitle("VOB");
                    ////FileCollection files = list.RootFolder.Folders.GetByUrl("/sites/sitename/shared documents/foldername").Files;
                    //FileCollection files = list.RootFolder.Folders.GetByUrl("Billing").Files;
                    //context.Load(files);
                    //context.ExecuteQuery();

                    //foreach (Microsoft.SharePoint.Client.File file in files)
                    //{
                    //    FileInformation fileinfo = Microsoft.SharePoint.Client.File.OpenBinaryDirect(context, file.ServerRelativeUrl);

                    //    context.ExecuteQuery();

                    //    using (FileStream filestream = new FileStream("C:" + "\\" + file.Name, FileMode.Create))
                    //    {
                    //        fileinfo.Stream.CopyTo(filestream);
                    //    }

                    //}

                }
            }
            catch (Exception ex)
            {
                var text = ex.Message;
            }
        }
    }
}
