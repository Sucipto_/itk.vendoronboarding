﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Newtonsoft.Json;
using System;
using Vendor_Onboarding_System.Localize;
using VOS.Models;
using VOS.Services;

namespace Vendor_Onboarding_System.Controllers
{
    public class VerifyNewVendorController : Controller
    {
        #region Session
        private string ConnectionString
        {
            get
            {
                try
                {
                    return JsonConvert.DeserializeObject<string>(HttpContext.Session.GetString("ConnectionString"));
                }
                catch
                {
                    return null;
                }
            }
        }

        private dbVendorAkun_Model CurrLoginUser
        {
            get
            {
                try
                {
                    return JsonConvert.DeserializeObject<dbVendorAkun_Model>(HttpContext.Session.GetString("CurrLoginUser"));
                }
                catch
                {
                    return null;
                }
            }
        }

        private List<dbVendorAkun_Model> AkunList
        {
            get
            {
                try
                {
                    return JsonConvert.DeserializeObject<List<dbVendorAkun_Model>>(HttpContext.Session.GetString("AkunList"));
                }
                catch
                {
                    return null;
                }
            }
        }

        private string EmailFrom
        {
            get
            {
                try
                {
                    return JsonConvert.DeserializeObject<string>(HttpContext.Session.GetString("EmailFrom"));
                }
                catch
                {
                    return null;
                }
            }
        }

        private string PasswordFrom
        {
            get
            {
                try
                {
                    return JsonConvert.DeserializeObject<string>(HttpContext.Session.GetString("PasswordFrom"));
                }
                catch
                {
                    return null;
                }
            }
        }

        private string SMTP
        {
            get
            {
                try
                {
                    return JsonConvert.DeserializeObject<string>(HttpContext.Session.GetString("SMTP"));
                }
                catch
                {
                    return null;
                }
            }
        }

        private string Port
        {
            get
            {
                try
                {
                    return JsonConvert.DeserializeObject<string>(HttpContext.Session.GetString("Port"));
                }
                catch
                {
                    return null;
                }
            }
        }
        #endregion

        private readonly IStringLocalizer<Resource> _stringLocalizer;
        private readonly ILogger<HomeController> _logger;
        private readonly IConfiguration _config;

        CompanyProfileService mCompanyProfileService = new CompanyProfileService();
        MailService mailService = new MailService();
        private static Random random = new Random();


        public VerifyNewVendorController(ILogger<HomeController> logger, IConfiguration config, IStringLocalizer<Resource> stringLocalizer)
        {
            _logger = logger;
            _config = config;
            _stringLocalizer = stringLocalizer;
        }

        public IActionResult Index()
        {
            ViewBag.Local = _stringLocalizer;
            if (CurrLoginUser == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                ViewBag.Login = true;
                ViewBag.Category = CurrLoginUser.Category;
            }
            return View();
        }

        public JsonResult GetNewVendorRegistration()
        {
            var data = mCompanyProfileService.GetNewVendorRegistration(ConnectionString);
            data.ForEach(x => x.StatusCheckBox = false);
            HttpContext.Session.SetString("AkunList", JsonConvert.SerializeObject(data));
            return Json(data);
        }
        public JsonResult GetSelectedCheckBox(string vendID, string status)
        {
            List<dbVendorAkun_Model> tes = new();
            if (AkunList != null && AkunList.Count() > 0)
            {
                tes = AkunList;
                //kalo checked = on
                if (status == "on")
                {
                    tes.SingleOrDefault(x => x.VendID == vendID).StatusCheckBox = true;
                }
                else
                {
                    tes.SingleOrDefault(x => x.VendID == vendID).StatusCheckBox = false;
                }
            }
            HttpContext.Session.SetString("AkunList", JsonConvert.SerializeObject(tes));
            return Json(AkunList);
        }
        public JsonResult ChangeVendorCategory(string vendID, string cat)
        {
            var akun = AkunList;
            akun.SingleOrDefault(x => x.VendID == vendID).Category = cat;
            mCompanyProfileService.UpdateVendorAkun(ConnectionString, akun.SingleOrDefault(x => x.VendID == vendID));
            //update vendor where akun.SingleOrDefault(x => x.VendID == vendID)
            HttpContext.Session.SetString("AkunList", JsonConvert.SerializeObject(akun));
            return Json(AkunList);

        }

        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public JsonResult SendVerificationEmail()
        {
            try
            {
                var listAkun = AkunList.ToList();
                foreach (var a in listAkun.Where(x => x.StatusCheckBox == true))
                {
                    //var success = false;
                    if (a.CompanyType == "Local")
                    {
                        mailService.SendVerificationEmailIndonesia(a.Email, Encryptor.Decrypt(a.Password), a.CompanyName, EmailFrom, PasswordFrom, SMTP, Convert.ToInt16(Port));
                    }
                    else
                    {
                        mailService.SendVerificationEmailEnglish(a.Email, Encryptor.Decrypt(a.Password), a.CompanyName, EmailFrom, PasswordFrom, SMTP, Convert.ToInt16(Port));
                    }
                    a.Flag = 1;
                    mCompanyProfileService.UpdateVendorAkun(ConnectionString, a);
                }
                return Json(new Response<string>
                {
                    Status = "Success"
                });
            }
            catch (Exception ex)
            {
                return Json(new Response<string>
                {
                    Status = "Failed",
                    Message = ex.Message
                });
            }
        }
    }
}