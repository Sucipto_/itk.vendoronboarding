//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ConsoleAppForEDMX
{
    using System;
    using System.Collections.Generic;
    
    public partial class dbVendorProductUnggulan
    {
        public string ProdID { get; set; }
        public string VendID { get; set; }
        public string GroupProd { get; set; }
        public string Nama { get; set; }
        public string Merk { get; set; }
        public string Sumber { get; set; }
        public string Pellanggan01 { get; set; }
        public string Pelanggan02 { get; set; }
        public string Pelanggan03 { get; set; }
        public string Pelanggan04 { get; set; }
        public string Pelanggan05 { get; set; }
    }
}
