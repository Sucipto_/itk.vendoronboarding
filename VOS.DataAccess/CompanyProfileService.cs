﻿using Dapper;
using System.Data;
using System.Data.SqlClient;
using VOS.Models;

namespace VOS.Services
{
    public class CompanyProfileService
    {
        #region CREATE
        //Add Vendor Akun
        public int RegisterUser(string ConnectionString, dbVendorAkun_Model model)
        {
            var query = @"INSERT INTO [dbo].[dbVendorAkun]
            ([Email]
            ,[Password]
            ,[CompanyName]
            ,[Website]
            ,[PICName]
            ,[Category]
            ,[BentukUsaha]
            ,[CompanyType]
            ,[NPWP]
            ,[TaxID]
            ,[Address]
            ,[Negara]
            ,[Provinsi]
            ,[Kota]
            ,[Kec]
            ,[ZipCode]
            ,[NoTelp]
            ,[PICEmail]
            ,[PICPhone]
            ,[Flag]
            ,[SalesID])
        VALUES
            (@Email
            ,@Password
            ,@CompanyName
            ,@Website
            ,@PICName
            ,@Category
            ,@BentukUsaha
            ,@CompanyType
            ,@NPWP
            ,@TaxID
            ,@Address
            ,@Negara
            ,@Provinsi
            ,@Kota
            ,@Kec
            ,@ZipCode
            ,@NoTelp
            ,@PICEmail
            ,@PICPhone
            ,@Flag
            ,@SalesID)";

            var parameters = new DynamicParameters();
            parameters.Add("Email", model.Email, DbType.String);
            parameters.Add("Password", model.Password, DbType.String);
            parameters.Add("CompanyName", model.CompanyName, DbType.String);
            parameters.Add("Website", model.Website, DbType.String);
            parameters.Add("PICName", model.PICName, DbType.String);
            parameters.Add("Category", model.Category, DbType.String);
            parameters.Add("BentukUsaha", model.BentukUsaha, DbType.String);
            parameters.Add("CompanyType", model.CompanyType, DbType.String);
            parameters.Add("NPWP", model.NPWP, DbType.String);
            parameters.Add("TaxID", model.TaxID, DbType.String);
            parameters.Add("Address", model.Address, DbType.String);
            parameters.Add("Negara", model.Negara, DbType.String);
            parameters.Add("Provinsi", model.Provinsi, DbType.String);
            parameters.Add("Kota", model.Kota, DbType.String);
            parameters.Add("Kec", model.Kec, DbType.String);
            parameters.Add("ZipCode", model.ZipCode, DbType.String);
            parameters.Add("NoTelp", model.NoTelp, DbType.String);
            parameters.Add("PICEmail", model.PICEmail, DbType.String);
            parameters.Add("PICPhone", model.PICPhone, DbType.String);
            parameters.Add("Flag", model.Flag, DbType.Int16);
            parameters.Add("SalesID", model.SalesID, DbType.String);

            return new SqlConnection(ConnectionString).Execute(query, parameters);
        }

        //Add Legal
        public int AddLegal(string ConnectionString, dbLegal_Model model)
        {
            var query = @"INSERT INTO [dbo].[dbLegal]
            ([VendID]
            ,[TypeAkta]
            ,[NoAkta]
            ,[TglPembuatan]
            ,[NotarisName]
            ,[NotarisAddress]
            ,[AttachmentAkta]
            ,[NomorSK]
            ,[DateSK]
            ,[AttachmentSK])
        VALUES
            (@VendID
            ,@TypeAkta
            ,@NoAkta
            ,@TglPembuatan
            ,@NotarisName
            ,@NotarisAddress
            ,@AttachmentAkta
            ,@NomorSK
            ,@DateSK
            ,@AttachmentSK)";

            var parameters = new DynamicParameters();
            parameters.Add("VendID", model.VendID, DbType.String);
            parameters.Add("TypeAkta", model.TypeAkta, DbType.String);
            parameters.Add("NoAkta", model.NoAkta, DbType.String);
            parameters.Add("TglPembuatan", model.TglPembuatan, DbType.DateTime);
            parameters.Add("NotarisName", model.NotarisName, DbType.String);
            parameters.Add("NotarisAddress", model.NotarisAddress, DbType.String);
            parameters.Add("AttachmentAkta", model.AttachmentAkta, DbType.String);
            parameters.Add("NomorSK", model.NomorSK, DbType.String);
            parameters.Add("DateSK", model.DateSK, DbType.DateTime);
            parameters.Add("AttachmentSK", model.AttachmentSK, DbType.String);

            return new SqlConnection(ConnectionString).Execute(query, parameters);
        }

        //Add License
        public int AddLicense(string ConnectionString, dbLicense_Model model)
        {
            var query = @"INSERT INTO [dbo].[dbLicense]
            ([LegalID]
            ,[TypeIjin]
            ,[Penerbit]
            ,[TglPembuatan]
            ,[TglKadaluarsa]
            ,[AttachmentIjin])
        VALUES
            (@LegalID,
            @TypeIjin,
            @Penerbit,
            @TglPembuatan,
            @TglKadaluarsa,
            @AttachmentIjin)";

            var parameters = new DynamicParameters();
            parameters.Add("LegalID", model.LegalID, DbType.String);
            parameters.Add("TypeIjin", model.TypeIjin, DbType.String);
            parameters.Add("Penerbit", model.Penerbit, DbType.String);
            parameters.Add("TglPembuatan", model.TglPembuatan, DbType.DateTime);
            parameters.Add("TglKadaluarsa", model.TglKadaluarsa, DbType.DateTime);
            parameters.Add("AttachmentIjin", model.AttachmentIjin, DbType.String);

            return new SqlConnection(ConnectionString).Execute(query, parameters);
        }

        //Add Sertifikat
        public int AddSertifikat(string ConnectionString, dbSertifikat_Model model)
        {
            var query = @"INSERT INTO [dbo].[dbSertifikat]
            ([TypeSertifikat]
            ,[Nama]
            ,[Penerbit]
            ,[TglPembuatan]
            ,[TglKadaluarsa]
            ,[AttachmentSertifikat]
            ,[Nomor])
        VALUES
            (@TypeSertifikat
            ,@Nama
            ,@Penerbit
            ,@TglPembuatan
            ,@TglKadaluarsa
            ,@AttachmentSertifikat
            ,@Nomor)";

            var parameters = new DynamicParameters();
            parameters.Add("TypeSertifikat", model.TypeSertifikat, DbType.String);
            parameters.Add("Nama", model.Nama, DbType.String);
            parameters.Add("Penerbit", model.Penerbit, DbType.String);
            parameters.Add("TglPembuatan", model.TglPembuatan, DbType.DateTime);
            parameters.Add("TglKadaluarsa", model.TglKadaluarsa, DbType.DateTime);
            parameters.Add("AttachmentSertifikat", model.AttachmentSertifikat, DbType.String);
            parameters.Add("Nomor", model.Nomor, DbType.String);

            return new SqlConnection(ConnectionString).Execute(query, parameters);
        }

        //Add Bank
        public int AddBank(string ConnectionString, dbBank_Model model)
        {
            var query = @"INSERT INTO [dbo].[dbBank]
            ([Nama]
            ,[CabangBank]
            ,[NoRek]
            ,[NamaRek]
            ,[MataUang]
            ,[CountryName])
        VALUES
            (@Nama,
            @CabangBank,
            @NoRek,
            @NamaRek,
            @MataUang,
            @CountryName)";

            var parameters = new DynamicParameters();
            parameters.Add("Nama", model.Nama, DbType.String);
            parameters.Add("CabangBank", model.CabangBank, DbType.String);
            parameters.Add("NoRek", model.NoRek, DbType.String);
            parameters.Add("NamaRek", model.NamaRek, DbType.String);
            parameters.Add("MataUang", model.MataUang, DbType.String);
            parameters.Add("CountryName", model.CountryName, DbType.String);

            return new SqlConnection(ConnectionString).Execute(query, parameters);
        }

        public int AddPengurus(string ConnectionString, dbPengurus_Model model)
        {
            var query = @"INSERT INTO [dbo].[dbPengurus]
            ([VendID]
            ,[Posisi]
            ,[NamaLengkap]
            ,[Email]
            ,[PhoneNumber])
        VALUES
            (@VendID,
            @Posisi,
            @NamaLengkap,
            @Email,
            @PhoneNumber)";

            var parameters = new DynamicParameters();
            parameters.Add("VendID", model.VendID, DbType.String);
            parameters.Add("Posisi", model.Posisi, DbType.String);
            parameters.Add("NamaLengkap", model.NamaLengkap, DbType.String);
            parameters.Add("Email", model.Email, DbType.String);
            parameters.Add("PhoneNumber", model.PhoneNumber, DbType.String);

            return new SqlConnection(ConnectionString).Execute(query, parameters);
        }

        public int AddProdUnggul(string ConnectionString, dbProdUnggul_Model model)
        {
            var query = @"INSERT INTO [dbo].[dbProdUnggul]
            ([VendID]
            ,[ProdName]
            ,[ProdGroup]
            ,[Merek]
            ,[Asal]
            ,[Pelanggan1]
            ,[Pelanggan2]
            ,[Pelanggan3]
            ,[Pelanggan4]
            ,[Pelanggan5]
            ,[AttachCompanyProfiles]
            ,[AttachKatalog]
            ,[Catatan]
            )
        VALUES
            (@VendID,
            @ProdName,
            @ProdGroup,
            @Merek,
            @Asal,
            @Pelanggan1,
            @Pelanggan2,
            @Pelanggan3,
            @Pelanggan4,
            @Pelanggan5,
            @AttachCompanyProfiles,
            @AttachKatalog,
            @Catatan)";

            var parameters = new DynamicParameters();
            parameters.Add("VendID", model.VendID, DbType.String);
            parameters.Add("ProdName", model.ProdName, DbType.String);
            parameters.Add("ProdGroup", model.ProdGroup, DbType.String);
            parameters.Add("Merek", model.Merek, DbType.String);
            parameters.Add("Asal", model.Asal, DbType.String);
            parameters.Add("Pelanggan1", model.Pelanggan1, DbType.String);
            parameters.Add("Pelanggan2", model.Pelanggan2, DbType.String);
            parameters.Add("Pelanggan3", model.Pelanggan3, DbType.String);
            parameters.Add("Pelanggan4", model.Pelanggan4, DbType.String);
            parameters.Add("Pelanggan5", model.Pelanggan5, DbType.String);
            parameters.Add("AttachCompanyProfiles", model.AttachCompanyProfiles, DbType.String);
            parameters.Add("AttachKatalog", model.AttachKatalog, DbType.String);
            parameters.Add("Catatan", model.Catatan, DbType.String);

            return new SqlConnection(ConnectionString).Execute(query, parameters);
        }

        public int AddNotes(string ConnectionString, dbNotes_Model model)
        {
            var query = @"INSERT INTO [dbo].[dbNotes]
            ([VendID]
            ,[CompanyProfile]
            ,[Katalog]
            ,[Catatan])
        VALUES
            (@VendID
            ,@CompanyProfile
            ,@Katalog
            ,@Katalog)";

            var parameters = new DynamicParameters();
            parameters.Add("VendID", model.VendID, DbType.String);
            parameters.Add("CompanyProfile", model.CompanyProfile, DbType.String);
            parameters.Add("Katalog", model.Katalog, DbType.String);
            parameters.Add("Katalog", model.Katalog, DbType.String);

            return new SqlConnection(ConnectionString).Execute(query, parameters);
        }

        #endregion

        #region READ
        //Login Function
        public dbVendorAkun_Model LoginUser(string ConnectionString, string Email, string Password)
        {
            var query = @"SELECT * FROM [dbo].[dbVendorAkun] WHERE Email='" + Email + "' AND Password='" + Password + "'";
            return new SqlConnection(ConnectionString).QueryFirstOrDefault<dbVendorAkun_Model>(query);
        }

        //Get CompanyType
        //public dbVendorAkun_Model GetCompanyTypebyVendID(string ConnectionString, string VendID)
        //{
        //    var query = @"";
        //    return new SqlConnection(ConnectionString).QueryFirstOrDefault<dbVendorAkun_Model>(query);
        //}

        //Populate New Vendor Registration
        public List<dbVendorAkun_Model> GetNewVendorRegistration(string ConnectionString)
        {
            var query = @"SELECT *
                        FROM [dbo].[dbVendorAkun]
                        WHERE Flag = 0";
            return new SqlConnection(ConnectionString).Query<dbVendorAkun_Model>(query).ToList();
        }

        //Populate Vendor Master
        public List<dbVendorAkun_Model> GetVendorMaster(string ConnectionString)
        {
            var query = @"SELECT [VendID]
                          ,[CompanyName]
                          ,[PICName]
                          ,[Email]
                    FROM [dbo].[dbVendorAkun]
                    WHERE Category = 'Principles'";

            return new SqlConnection(ConnectionString).Query<dbVendorAkun_Model>(query).ToList();
        }

        //Count Principles
        public int CountGeneral(string ConnectionString, string Variable)
        {
            var query = @"SELECT COUNT(*)
                        FROM dbVendorAkun
                        Where Category = '" + Variable + "'";

            return new SqlConnection(ConnectionString).QuerySingle<int>(query);
        }

        //Get All Principles
        public List<dbVendorAkun_Model> GetAllPrinciples(string ConnectionString)
        {
            var query = @"SELECT VendID, CompanyName, PICName, Email FROM dbVendorAkun
                        Where Category = 'Principles'";

            return new SqlConnection(ConnectionString).Query<dbVendorAkun_Model>(query).ToList();
        }

        //Get All Distributor
        public List<dbVendorAkun_Model> GetAllDistributor(string ConnectionString)
        {
            var query = @"SELECT VendID, CompanyName, PICName, Email FROM dbVendorAkun
                        Where Category = 'Distributor'";

            return new SqlConnection(ConnectionString).Query<dbVendorAkun_Model>(query).ToList();
        }

        //Get All System Integrator
        public List<dbVendorAkun_Model> GetAllSystemIntegrator(string ConnectionString)
        {
            var query = @"SELECT VendID, CompanyName, PICName, Email FROM dbVendorAkun
                        Where Category = 'System Integrator'";

            return new SqlConnection(ConnectionString).Query<dbVendorAkun_Model>(query).ToList();
        }

        //Get All System Local Vendor
        public List<dbVendorAkun_Model> GetAllLocalVendor(string ConnectionString)
        {
            var query = @"SELECT VendID, CompanyName, PICName, Email FROM dbVendorAkun
                        Where Category = 'Local Vendor'";

            return new SqlConnection(ConnectionString).Query<dbVendorAkun_Model>(query).ToList();
        }

        //Populate Data Utama
        public dbVendorAkun_Model GetAllVendorAkun(string ConnectionString)
        {
            var query = @"SELECT * FROM [dbo].[dbVendorAkun]";
            return new SqlConnection(ConnectionString).QueryFirst<dbVendorAkun_Model>(query);
        }

        public dbVendorAkun_Model GetVendorAkunByVendID(string ConnectionString, string VendID)
        {
            var query = @"SELECT * FROM [dbo].[dbVendorAkun] WHERE VendID='" + VendID + "'";
            return new SqlConnection(ConnectionString).QueryFirstOrDefault<dbVendorAkun_Model>(query);
        }

        //Populate Legal, License, Sertifikat for Legal Page
        public dbLegal_Model GetLegalIDByVendID(string ConnectionString, string VendID)
        {
            var query = @"SELECT LegalID FROM [dbo].[dbLegal] WHERE VendID='" + VendID + "'";
            return new SqlConnection(ConnectionString).QueryFirstOrDefault<dbLegal_Model>(query);
        }
        public List<dbLicense_Model> GetAllLicense(string ConnectionString)
        {
            var query = @"SELECT * FROM [dbo].[dbLicense]";
            return new SqlConnection(ConnectionString).Query<dbLicense_Model>(query).ToList();
        }
        public dbLicense_Model GetLicenseByVendID(string ConnectionString, string VendID)
        {
            var query = @"SELECT * FROM [dbo].[dbLicense] WHERE VendID='" + VendID + "'";
            return new SqlConnection(ConnectionString).QueryFirstOrDefault<dbLicense_Model>(query);
        }

        //Populate Sertifikat for Legal Page
        public dbSertifikat_Model GetSertifikatByVendID(string ConnectionString, string VendID)
        {
            var query = @"SELECT * FROM [dbo].[dbSertifikat] WHERE VendID='" + VendID + "'";
            return new SqlConnection(ConnectionString).QueryFirstOrDefault<dbSertifikat_Model>(query);
        }

        //Populate Informasi Bank for Keuangan Page
        public dbKeuangan_Model GetKeuanganIDByVendID(string ConnectionString, string VendID)
        {
            var query = @"SELECT [KeuanganID]
                        FROM [dbo].[dbKeuangan]
                        WHERE VendID ='" + VendID + "'";

            return new SqlConnection(ConnectionString).QueryFirstOrDefault(query);
        }
        public List<dbBank_Model> GetAllBankByKeuanganID(string ConnectionString, string KeuanganID)
        {
            var query = @"SELECT [Nama]
                        ,[CabangBank]
                        ,[NoRek]
                        ,[NamaRek]
                        ,[MataUang]
                        ,[CountryName]
                        FROM [dbo].[dbBank]
                        WHERE [KeuanganID] = '" + KeuanganID + "'";

            return new SqlConnection(ConnectionString).Query<dbBank_Model>(query).ToList();
        }

        //Populate Pengurus for Pengurus Page
        public List<dbPengurus_Model> GetAllPengurus(string ConnectionString, string VendID)
        {
            var query = @"SELECT [Posisi]
                        ,[NamaLengkap]
                        ,[Email]
                        ,[PhoneNumber]
                        FROM [dbo].[dbPengurus]
                        WHERE VendID = '" + VendID + "'";

            return new SqlConnection(ConnectionString).Query<dbPengurus_Model>(query).ToList();
        }

        //Populate Keuangan
        public List<dbBank_Model> GetAllBank(string ConnectionString)
        {
            var query = @"SELECT * FROM [dbo].[dbBank]";
            return new SqlConnection(ConnectionString).Query<dbBank_Model>(query).ToList();
        }
        public dbBank_Model GeBankByID(string ConnectionString, string BankID)
        {
            var query = @"SELECT * FROM [dbo].[dbBank] WHERE BankID='" + BankID + "'";
            return new SqlConnection(ConnectionString).QueryFirst<dbBank_Model>(query);
        }
        public dbBank_Model GetBankByVendID(string ConnectionString, string VendID)
        {
            var query = @"SELECT * FROM [dbo].[dbBank] WHERE VendID='" + VendID + "'";
            return new SqlConnection(ConnectionString).QueryFirstOrDefault<dbBank_Model>(query);
        }

        //Populate Pengurus(BOARD)
        public List<dbPengurus_Model> GetAllPengurus(string ConnectionString)
        {
            var query = @"SELECT * FROM [dbo].[dbPengurus]";
            return new SqlConnection(ConnectionString).Query<dbPengurus_Model>(query).ToList();
        }
        public dbPengurus_Model GetPengurusByID(string ConnectionString, string BoardID)
        {
            var query = @"SELECT * FROM [dbo].[dbPengurus] WHERE BoardID='" + BoardID + "'";
            return new SqlConnection(ConnectionString).QueryFirst<dbPengurus_Model>(query);
        }
        public dbPengurus_Model GetPengurusByVendID(string ConnectionString, string VendID)
        {
            var query = @"SELECT * FROM [dbo].[dbPengurus] WHERE VendID='" + VendID + "'";
            return new SqlConnection(ConnectionString).QueryFirstOrDefault<dbPengurus_Model>(query);
        }

        //Populate Product Unggulan
        public List<dbProdUnggul_Model> GetAllProdUnggul(string ConnectionString)
        {
            var query = @"SELECT * FROM [dbo].[dbProdUnggul]";
            return new SqlConnection(ConnectionString).Query<dbProdUnggul_Model>(query).ToList();
        }
        public dbProdUnggul_Model GetProdUnggulByID(string ConnectionString, string ProdID)
        {
            var query = @"SELECT * FROM [dbo].[dbProdUnggul] WHERE ProdID='" + ProdID + "'";
            return new SqlConnection(ConnectionString).QueryFirst<dbProdUnggul_Model>(query);

        }
        public List<dbProdUnggul_Model> GetProdUnggulByVendID(string ConnectionString, string VendID)
        {
            var query = @"SELECT * FROM [dbo].[dbProdUnggul] WHERE VendID='" + VendID + "'";
            return new SqlConnection(ConnectionString).Query<dbProdUnggul_Model>(query).ToList();
        }

        //Populate Notes for Notes(CATATAN) Page
        public List<dbNotes_Model> GetAllNotesByVendID(string ConnectionString, string VendID)
        {
            var query = @"SELECT * FROM [dbo].[dbNotes] WHERE VendID='" + VendID + "'";
            return new SqlConnection(ConnectionString).Query<dbNotes_Model>(query).ToList();
        }
        #endregion

        #region UPDATE
        public int UpdateVendorAkun(string ConnectionString, dbVendorAkun_Model model)
        {
            var query = @"UPDATE [dbo].[dbVendorAkun]
            SET [Email] = @Email
                ,[Password] = @Password
                ,[CompanyName] = @CompanyName
                ,[Website] = @Website
                ,[PICName] = @PICName
                ,[Category] = @Category
                ,[BentukUsaha] = @BentukUsaha
                ,[NPWP] = @NPWP
                ,[TaxID] = @TaxID
                ,[Address] = @Address
                ,[Negara] = @Negara
                ,[Provinsi] = @Provinsi
                ,[Kota] = @Kota
                ,[Kec] = @Kec
                ,[ZipCode] = @ZipCode
                ,[NoTelp] = @NoTelp
                ,[PICEmail] = @PICEmail
                ,[PICPhone] = @PICPhone
                ,[Flag] = @Flag
                ,[SalesID] = @SalesID
            WHERE VendID = @VendID";

            var parameters = new DynamicParameters();
            parameters.Add("Email", model.Email, DbType.String);
            parameters.Add("Password", model.Password, DbType.String);
            parameters.Add("CompanyName", model.CompanyName, DbType.String);
            parameters.Add("Website", model.Website, DbType.String);
            parameters.Add("PICName", model.PICName, DbType.String);
            parameters.Add("Category", model.Category, DbType.String);
            parameters.Add("BentukUsaha", model.BentukUsaha, DbType.String);
            parameters.Add("NPWP", model.NPWP, DbType.String);
            parameters.Add("TaxID", model.TaxID, DbType.String);
            parameters.Add("Address", model.Address, DbType.String);
            parameters.Add("Negara", model.Negara, DbType.String);
            parameters.Add("Provinsi", model.Provinsi, DbType.String);
            parameters.Add("Kota", model.Kota, DbType.String);
            parameters.Add("Kec", model.Kec, DbType.String);
            parameters.Add("ZipCode", model.ZipCode, DbType.String);
            parameters.Add("NoTelp", model.NoTelp, DbType.String);
            parameters.Add("PICEmail", model.PICEmail, DbType.String);
            parameters.Add("PICPhone", model.PICPhone, DbType.String);
            parameters.Add("Flag", model.Flag, DbType.Int16);
            parameters.Add("SalesID", model.SalesID, DbType.String);
            parameters.Add("VendID", model.VendID, DbType.String);

            return new SqlConnection(ConnectionString).Execute(query, parameters);
        }

        public int UpdateLicense(string ConnectionString, dbLicense_Model model)
        {
            var query = @"UPDATE [dbo].[dbLicense]
            SET [TypeIjin] = @TypeIjin
                ,[Penerbit] = @Penerbit
                ,[TglPembuatan] = @TglPembuatan
                ,[TglKadaluarsa] = @TglKadaluarsa
                ,[AttachmentIjin] = @AttachmentIjin
            WHERE LicenID = @LicenID";

            var parameters = new DynamicParameters();
            parameters.Add("TypeIjin", model.TypeIjin, DbType.String);
            parameters.Add("Penerbit", model.Penerbit, DbType.String);
            parameters.Add("TglPembuatan", model.TglPembuatan, DbType.DateTime);
            parameters.Add("TglKadaluarsa", model.TglKadaluarsa, DbType.DateTime);
            parameters.Add("AttachmentIjin", model.AttachmentIjin, DbType.String);

            return new SqlConnection(ConnectionString).Execute(query, parameters);
        }

        public int UpdateBank(string ConnectionString, dbBank_Model model)
        {
            var query = @"UPDATE [dbo].[dbBank]
            SET [Nama] = @Nama
                ,[CabangBank] = @CabangBank
                ,[NoRek] = @NoRek
                ,[NamaRek] = @NamaRek
                ,[MataUang] = @MataUang
                ,[CountryName] = @CountryName
            WHERE BankID = @BankID";

            var parameters = new DynamicParameters();
            parameters.Add("Nama", model.Nama, DbType.String);
            parameters.Add("CabangBank", model.CabangBank, DbType.String);
            parameters.Add("NoRek", model.NoRek, DbType.String);
            parameters.Add("NamaRek", model.NamaRek, DbType.String);
            parameters.Add("MataUang", model.MataUang, DbType.String);
            parameters.Add("CountryName", model.CountryName, DbType.String);

            return new SqlConnection(ConnectionString).Execute(query, parameters);
        }

        public int UpdatePengurus(string ConnectionString, dbPengurus_Model model)
        {
            var query = @"UPDATE [dbo].[dbPengurus]
            SET [Posisi] = @Posisi
                ,[NamaLengkap] = @NamaLengkap
                ,[Email] = @Email
                ,[PhoneNumber] = @PhoneNumber
            WHERE BoardID = @BoardID";

            var parameters = new DynamicParameters();
            parameters.Add("Posisi", model.Posisi, DbType.String);
            parameters.Add("NamaLengkap", model.NamaLengkap, DbType.String);
            parameters.Add("Email", model.Email, DbType.String);
            parameters.Add("PhoneNumber", model.PhoneNumber, DbType.String);

            return new SqlConnection(ConnectionString).Execute(query, parameters);
        }

        public int UpdateProdUnggul(string ConnectionString, dbProdUnggul_Model model)
        {
            var query = @"UPDATE [dbo].[dbProdUnggul]
            SET [ProdName] = @ProdName
                ,[ProdGroup] = @ProdGroup
                ,[Merek] = @Merek
                ,[Asal] = @Asal
                ,[Pelanggan1] = @Pelanggan1
                ,[Pelanggan2] = @Pelanggan2
                ,[Pelanggan3] = @Pelanggan3
                ,[Pelanggan4] = @Pelanggan4
                ,[Pelanggan5] = @Pelanggan5
                ,[AttachCompanyProfiles] = @AttachCompanyProfiles
                ,[AttachKatalog] = @<AttachKatalog, varchar(max),>
                ,[Catatan] = @Catatan
            WHERE ProdID = @ProdID";

            var parameters = new DynamicParameters();
            parameters.Add("ProdName", model.ProdName, DbType.String);
            parameters.Add("ProdGroup", model.ProdGroup, DbType.String);
            parameters.Add("Merek", model.Merek, DbType.String);
            parameters.Add("Asal", model.Asal, DbType.String);
            parameters.Add("Pelanggan1", model.Pelanggan1, DbType.String);
            parameters.Add("Pelanggan2", model.Pelanggan2, DbType.String);
            parameters.Add("Pelanggan3", model.Pelanggan3, DbType.String);
            parameters.Add("Pelanggan4", model.Pelanggan4, DbType.String);
            parameters.Add("Pelanggan5", model.Pelanggan5, DbType.String);
            parameters.Add("AttachCompanyProfiles", model.AttachCompanyProfiles, DbType.String);
            parameters.Add("AttachKatalog", model.AttachKatalog, DbType.String);
            parameters.Add("Catatan", model.Catatan, DbType.String);

            return new SqlConnection(ConnectionString).Execute(query, parameters);
        }

        public int UpdateNotes(string ConnectionString, dbNotes_Model model)
        {
            var query = @"UPDATE [dbo].[dbNotes]
            SET [CompanyProfile] = @CompanyProfile
                ,[Katalog] = @CompanyProfile
                ,[Catatan] = @Catatan
            WHERE NotesID = @NotesID";

            var parameters = new DynamicParameters();
            parameters.Add("CompanyProfile", model.CompanyProfile, DbType.String);
            parameters.Add("Katalog", model.Katalog, DbType.String);
            parameters.Add("Catatan", model.Catatan, DbType.String);

            return new SqlConnection(ConnectionString).Execute(query, parameters);
        }
        #endregion

        #region DELETE

        #endregion
    }
}
