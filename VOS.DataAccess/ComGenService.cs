﻿using Dapper;
using System.Data.SqlClient;
using VOS.Models;

namespace VOS.Services
{
    public class ComGenService
    {
        public ComGen_Model GetComGenByVariable(string ConnectionString, string Variable)
        {
            var query = @"SELECT * FROM ComGen WHERE Variable = '" + Variable + "'";
            return new SqlConnection(ConnectionString).QueryFirstOrDefault<ComGen_Model>(query);
        }
        public List<ComGen_Model> GetComGenByVariableLikePercent(string ConnectionString, string Variable)
        {
            var query = @"SELECT * FROM ComGen WHERE Variable LIKE '" + Variable + "%'";
            return new SqlConnection(ConnectionString).Query<ComGen_Model>(query).ToList();
        }
    }
}
