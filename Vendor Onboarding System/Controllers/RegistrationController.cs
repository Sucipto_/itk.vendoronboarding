﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Newtonsoft.Json;
using System.Diagnostics;
using Vendor_Onboarding_System.Localize;
using Vendor_Onboarding_System.Models;
using VOS.Models;
using VOS.Services;

namespace Vendor_Onboarding_System.Controllers
{
    public class RegistrationController : Controller
    {

        #region Session
        private string ConnectionString
        {
            get
            {
                try
                {
                    return JsonConvert.DeserializeObject<string>(HttpContext.Session.GetString("ConnectionString"));
                }
                catch
                {
                    return null;
                }
            }
        }

        private dbVendorAkun_Model CurrLoginUser
        {
            get
            {
                try
                {
                    return JsonConvert.DeserializeObject<dbVendorAkun_Model>(HttpContext.Session.GetString("CurrLoginUser"));
                }
                catch
                {
                    return null;
                }
            }
        }
        #endregion

        private readonly IStringLocalizer<Resource> _stringLocalizer;
        private readonly ILogger<RegistrationController> _logger;
        private readonly IConfiguration _config;
        private static Random random = new Random();

        CompanyProfileService mCompanyProfileService = new CompanyProfileService();

        public RegistrationController(ILogger<RegistrationController> logger, IConfiguration config, IStringLocalizer<Resource> stringLocalizer)
        {
            _logger = logger;
            _config = config;
            _stringLocalizer = stringLocalizer;
        }

        public IActionResult Index()
        {
            HttpContext.Session.SetString("ConnectionString", JsonConvert.SerializeObject(_config.GetValue<string>("DBConnection")));
            HttpContext.Session.SetString("EmailFrom", JsonConvert.SerializeObject(_config.GetValue<string>("EmailFrom")));
            HttpContext.Session.SetString("PasswordFrom", JsonConvert.SerializeObject(_config.GetValue<string>("PasswordFrom")));
            HttpContext.Session.SetString("SMTP", JsonConvert.SerializeObject(_config.GetValue<string>("SMTP")));
            HttpContext.Session.SetString("Port", JsonConvert.SerializeObject(_config.GetValue<int>("Port")));

            ViewBag.Local = _stringLocalizer;
            if (CurrLoginUser == null)
            {
                ViewBag.Login = false;
            }
            else
            {
                ViewBag.Login = true;
                ViewBag.Category = CurrLoginUser.Category;
            }
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        #region Get Country Detail
        CountryDetailService mCountryDetail = new CountryDetailService();
        public JsonResult GetAllCountry()
        {
            try
            {
                return Json(new Response<List<dbCountry_Model>>
                {
                    Status = "Success",
                    Value = mCountryDetail.GetAllCountry(ConnectionString)
                });
            }
            catch
            {
                return Json(new Response<string>
                {
                    Status = "Failed",
                    Message = "Failed to load country list."
                });
            }
        }
        public JsonResult GetAllStateByCountryID(int CountryID)
        {
            try
            {
                return Json(new Response<List<dbProvince_Model>>
                {
                    Status = "Success",
                    Value = mCountryDetail.GetAllStateByCountryID(ConnectionString, CountryID)
                });
            }
            catch
            {
                return Json(new Response<string>
                {
                    Status = "Failed",
                    Message = "Failed to load state list."
                });
            }
        }

        public JsonResult GetAllCitiesByStateID(int StateID)
        {
            try
            {
                return Json(new Response<List<dbCity_Model>>
                {
                    Status = "Success",
                    Value = mCountryDetail.GetAllCitiesByStateID(ConnectionString, StateID)
                });
            }
            catch
            {
                return Json(new Response<string>
                {
                    Status = "Failed",
                    Message = "Failed to load city list."
                });
            }
        }
        #endregion

        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        public JsonResult Register(string Email, string CompanyName, string CompanyType, string Website, string PICName, string Category, string BentukUsaha, string NPWP, string TaxID, string Address, string Negara, string Provinsi, string Kota, string Kec, string ZipCode, string NoTelp)
        {

            try
            {
                mCompanyProfileService.RegisterUser(ConnectionString, new dbVendorAkun_Model()
                {
                    Email = Email,
                    Password = Encryptor.Encrypt(RandomString(15)),
                    CompanyName = CompanyName,
                    Website = Website,
                    PICName = PICName,
                    Category = Category,
                    BentukUsaha = BentukUsaha,
                    CompanyType = CompanyType,
                    NPWP = NPWP,
                    TaxID = TaxID,
                    Address = Address,
                    Negara = Negara,
                    Provinsi = Provinsi,
                    Kota = Kota,
                    Kec = Kec,
                    ZipCode = ZipCode,
                    NoTelp = NoTelp,
                    Flag = 0
                });

                return Json(new Response<string>()
                {
                    Status = "Success"
                });
            }
            catch (Exception ex)
            {
                var customMessage = ex.Message;
                if (customMessage.Contains("Cannot insert duplicate key row in object 'dbo.dbVendorAkun' with unique index 'IX_dbVendorAkun'."))
                {
                    customMessage = "Email already taken.";
                }
                return Json(new Response<string>()
                {
                    Status = "Failed",
                    Message = customMessage
                });
            }
        }

    }
}