﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using VOS.Models;
using VOS.Services;

namespace Vendor_Onboarding_System.Controllers
{
    public class LoginController : Controller
    {
        #region Session
        private string ConnectionString
        {
            get
            {
                try
                {
                    return JsonConvert.DeserializeObject<string>(HttpContext.Session.GetString("ConnectionString"));
                }
                catch
                {
                    return null;
                }
            }
        }

        private dbVendorAkun_Model CurrLoginUser
        {
            get
            {
                try
                {
                    return JsonConvert.DeserializeObject<dbVendorAkun_Model>(HttpContext.Session.GetString("CurrLoginUser"));
                }
                catch
                {
                    return null;
                }
            }
        }
        #endregion

        CompanyProfileService mUser = new CompanyProfileService();

        public JsonResult Login(string Email, string Password)
        {
            try
            {
                var login = mUser.LoginUser(ConnectionString, Email, Encryptor.Encrypt(Password));
                if (login == null)
                {
                    throw new Exception("Email / Password is Incorrect.");
                }
                HttpContext.Session.SetString("CurrLoginUser", JsonConvert.SerializeObject(login));
                return Json("Success");
            }
            catch (Exception ex)
            {
                return Json(ex.Message);
            }
        }
    }
}
