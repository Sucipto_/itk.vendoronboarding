//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ConsoleAppForEDMX
{
    using System;
    using System.Collections.Generic;
    
    public partial class dbVendorPengurusBoard
    {
        public string BoardID { get; set; }
        public string VendID { get; set; }
        public string Posisi { get; set; }
        public string Nama { get; set; }
        public string Email { get; set; }
        public Nullable<int> PhoneNumber { get; set; }
    }
}
