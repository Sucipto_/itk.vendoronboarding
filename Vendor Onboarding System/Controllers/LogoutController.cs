﻿using Microsoft.AspNetCore.Mvc;

namespace Vendor_Onboarding_System.Controllers
{
    public class LogoutController : Controller
    {
        public IActionResult Index()
        {
            HttpContext.Session.Clear();
            return RedirectToAction("Index", "Home");
        }
    }
}
