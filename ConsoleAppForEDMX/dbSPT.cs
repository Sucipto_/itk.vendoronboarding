//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ConsoleAppForEDMX
{
    using System;
    using System.Collections.Generic;
    
    public partial class dbSPT
    {
        public string SPTID { get; set; }
        public string PajakID { get; set; }
        public Nullable<int> Tahun { get; set; }
        public Nullable<System.DateTime> TglLapor { get; set; }
        public string AttachSPT { get; set; }
        public string AttachDGTForm { get; set; }
        public string AttachBuktiLapor { get; set; }
    
        public virtual dbPajak dbPajak { get; set; }
    }
}
