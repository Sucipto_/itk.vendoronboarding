//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace VOS.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class dbBukpot_Model
    {
        public string id { get; set; }
        public string BukpotID { get; set; }
        public string VendID { get; set; }
        public string AttachBukpot { get; set; }
        public string DateTran { get; set; }
        public int CountingDownload { get; set; } 
    }
}
