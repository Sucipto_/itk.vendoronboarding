﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VOS.Models;

namespace VOS.Services
{
    public class PortalPajakService
    {
        #region Create
        public int AddBukpot(string ConnectionString, dbBukpot_Model model)
        {
            var query = @"INSERT INTO [dbo].[dbBukpot]
                       ([BukpotID]
                       ,[VendID]
                       ,[AttachBukpot]
                       ,[DateTran]
                       ,[CountingDownload])
                 VALUES
                       @BukpotID
                       ,@VendID
                       ,@AttachBukpot
                       ,@DateTran
                       ,@CountingDownload)";

            var parameters = new DynamicParameters();
            parameters.Add("BukpotID", model.BukpotID, DbType.String);
            parameters.Add("VendID", model.VendID, DbType.String);
            parameters.Add("AttachBukpot", model.AttachBukpot, DbType.String);
            parameters.Add("DateTran", model.DateTran, DbType.DateTime);
            parameters.Add("CountingDownload", model.CountingDownload, DbType.String);

            return new SqlConnection(ConnectionString).Execute(query, parameters);
        }
        #endregion

        #region Read

        //Populate Billing Portal for Vendor
        public List<dbBukpot_Model> GetAllBukpotByVendID(string ConnectionString, string VendID)
        {
            var query = @"SELECT *
                        FROM [dbo].[dbBukpot]
                        WHERE VendID = '" + VendID + "'";

            return new SqlConnection(ConnectionString).Query<dbBukpot_Model>(query).ToList();
        }

        //Populate All Biling Portal for Admin Intikom
        public List<dbBukpot_Model> GetAllBukpot(string ConnectionString)
        {
            var query = @"SELECT *
                        FROM [dbo].[dbBukpot]";

            return new SqlConnection(ConnectionString).Query<dbBukpot_Model>(query).ToList();
        }
        #endregion

        #region Update
        public int UpdateBukpot(string ConnectionString, string VendID, dbBukpot_Model model)
        {
            var query = @"UPDATE [dbo].[dbBukpot]
                       SET [BukpotID] = @BukpotID
                          ,[VendID] = @VendID
                          ,[AttachBukpot] = @AttachBukpot
                          ,[DateTran] = @DateTran
                          ,[CountingDownload] = @CountingDownload
                WHERE VendID = '" + VendID + "'";

            var parameters = new DynamicParameters();
            parameters.Add("BukpotID", model.BukpotID, DbType.String);
            parameters.Add("VendID", model.VendID, DbType.String);
            parameters.Add("AttachBukpot", model.AttachBukpot, DbType.String);
            parameters.Add("DateTran", model.DateTran, DbType.DateTime);
            parameters.Add("CountingDownload", model.CountingDownload, DbType.String);

            return new SqlConnection(ConnectionString).Execute(query, parameters);
        }
        #endregion

        #region Delete

        #endregion
    }
}
