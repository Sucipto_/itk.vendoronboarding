﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Newtonsoft.Json;
using Vendor_Onboarding_System.Localize;
using VOS.Models;
using VOS.Services;

namespace Vendor_Onboarding_System.Controllers
{
    public class CompanyProfilesController : Controller
    {
        #region Session
        private string ConnectionString
        {
            get
            {
                try
                {
                    return JsonConvert.DeserializeObject<string>(HttpContext.Session.GetString("ConnectionString"));
                }
                catch
                {
                    return null;
                }
            }
        }

        private dbVendorAkun_Model CurrLoginUser
        {
            get
            {
                try
                {
                    return JsonConvert.DeserializeObject<dbVendorAkun_Model>(HttpContext.Session.GetString("CurrLoginUser"));
                }
                catch
                {
                    return null;
                }
            }
        }
        #endregion

        private readonly IStringLocalizer<Resource> _stringLocalizer;
        private readonly ILogger<HomeController> _logger;
        private readonly IConfiguration _config;

        CompanyProfileService mCompanyProfile = new CompanyProfileService();

        public CompanyProfilesController(ILogger<HomeController> logger, IConfiguration config, IStringLocalizer<Resource> stringLocalizer)
        {
            _logger = logger;
            _config = config;
            _stringLocalizer = stringLocalizer;
        }

        public IActionResult Index()
        {
            ViewBag.Local = _stringLocalizer;
            if (CurrLoginUser == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                ViewBag.Login = true;
                ViewBag.Category = CurrLoginUser.Category;
            }
            return View();
        }

        #region CREATE
        public JsonResult SaveDataUtama(string VendID, string PICEmail, string PICPhone)
        {
            try
            {
                mCompanyProfile.UpdateVendorAkun(ConnectionString, new dbVendorAkun_Model()
                {
                    VendID = VendID,
                    PICEmail = PICEmail,
                    PICPhone = PICPhone
                });

                return Json(new Response<string>()
                {
                    Status = "Success"
                });

            }
            catch (Exception ex)
            {
                return Json(new Response<string>()
                {
                    Status = "Failed",
                    Message = ex.Message
                });
            }
        }

        public JsonResult GetLegalIDByVendID()
        {
            var LegalID = mCompanyProfile.GetLegalIDByVendID(ConnectionString, CurrLoginUser.VendID);
            return Json(LegalID);
        }

        public JsonResult SaveLicense(string TypeIjin, string Penerbit, DateTime TglPembuatan, DateTime TglKadaluarsa, string AttachmentIjin)
        {
            try
            {
                var LegalID = GetLegalIDByVendID();

                mCompanyProfile.AddLicense(ConnectionString, new dbLicense_Model()
                {
                    LegalID = Convert.ToString(LegalID),
                    TypeIjin = TypeIjin,
                    Penerbit = Penerbit,
                    TglPembuatan = TglPembuatan,
                    TglKadaluarsa = TglKadaluarsa,
                    AttachmentIjin = AttachmentIjin
                });

                return Json(new Response<string>
                {
                    Status = "Success"
                });
            }
            catch (Exception ex)
            {
                return Json(new Response<string>
                {
                    Status = "Failed",
                    Message = ex.Message
                });
            }
        }

        public JsonResult SaveSertifikat(string TypeSertifikat, string Nama, string Penerbit, DateTime TglPembuatan, DateTime TglKadaluarsa, string AttachmentSertifikat, string Nomor)
        {
            try
            {
                var LegalID = GetLegalIDByVendID();

                mCompanyProfile.AddSertifikat(ConnectionString, new dbSertifikat_Model()
                {
                    LegalID = Convert.ToString(LegalID),
                    TypeSertifikat = TypeSertifikat,
                    Nama = Nama,
                    Penerbit = Penerbit,
                    TglPembuatan = TglPembuatan,
                    TglKadaluarsa = TglKadaluarsa,
                    AttachmentSertifikat = AttachmentSertifikat,
                    Nomor = Nomor
                });

                return Json(new Response<string>()
                {
                    Status = "Success"
                });
            }
            catch (Exception ex)
            {
                return Json(new Response<string>()
                {
                    Status = "Failed",
                    Message = ex.Message
                });
            }
        }

        public JsonResult GetKeuanganIDByVendID()
        {
            var KeuanganID = mCompanyProfile.GetKeuanganIDByVendID(ConnectionString, CurrLoginUser.VendID);
            return Json(KeuanganID);
        }

        public JsonResult SaveBank(string Nama, string CabangBank, string NoRek, string NamaRek, string MataUang, string CountryName)
        {
            try
            {
                var KeuanganID = GetKeuanganIDByVendID();
                mCompanyProfile.AddBank(ConnectionString, new dbBank_Model()
                {
                    KeuanganID = Convert.ToString(KeuanganID),
                    Nama = Nama,
                    CabangBank = CabangBank,
                    NoRek = NoRek,
                    NamaRek = NamaRek,
                    MataUang = MataUang,
                    CountryName = CountryName
                });

                return Json(new Response<string>()
                {
                    Status = "Success"
                });
            }
            catch (Exception ex)
            {
                return Json(new Response<string>()
                {
                    Status = "Failed",
                    Message = ex.Message
                });
            }
        }

        public JsonResult SavePengurus(string Posisi, string NamaLengkap, string Email, string PhoneNumber)
        {
            try
            {
                mCompanyProfile.AddPengurus(ConnectionString, new dbPengurus_Model()
                {
                    VendID = CurrLoginUser.VendID,
                    Posisi = Posisi,
                    NamaLengkap = NamaLengkap,
                    Email = Email,
                    PhoneNumber = PhoneNumber
                });

                return Json(new Response<string>()
                {
                    Status = "Success"
                });
            }
            catch (Exception ex)
            {
                return Json(new Response<string>()
                {
                    Status = "Failed",
                    Message = ex.Message
                });
            }
        }

        public JsonResult SaveProdUnggul(string ProdName, string ProdGroup, string Merek, string Asal, string Pelanggan1, string Pelanggan2, string Pelanggan3, string Pelanggan4, string Pelanggan5, string AttachCompanyProfiles, string AttachKatalog, string Catatan)
        {
            try
            {
                mCompanyProfile.AddProdUnggul(ConnectionString, new dbProdUnggul_Model()
                {
                    VendID = CurrLoginUser.VendID,
                    ProdName = ProdName,
                    ProdGroup = ProdGroup,
                    Merek = Merek,
                    Asal = Asal,
                    Pelanggan1 = Pelanggan1,
                    Pelanggan2 = Pelanggan2,
                    Pelanggan3 = Pelanggan3,
                    Pelanggan4 = Pelanggan4,
                    Pelanggan5 = Pelanggan5,
                    AttachCompanyProfiles = AttachCompanyProfiles,
                    AttachKatalog = AttachKatalog,
                    Catatan = Catatan
                });

                return Json(new Response<string>()
                {
                    Status = "Success"
                });
            }
            catch (Exception ex)
            {
                return Json(new Response<string>()
                {
                    Status = "Failed",
                    Message = ex.Message
                });
            }
        }

        public JsonResult SaveNotes(string CompanyProfile, string Katalog, string Catatan)
        {
            try
            {
                mCompanyProfile.AddNotes(ConnectionString, new dbNotes_Model()
                {
                    VendID = CurrLoginUser.VendID,
                    CompanyProfile = CompanyProfile,
                    Katalog = Katalog,
                    Catatan = Catatan
                });

                return Json(new Response<string>()
                {
                    Status = "Success"
                });
            }
            catch (Exception ex)
            {
                return Json(new Response<string>()
                {
                    Status = "Failed",
                    Message = ex.Message
                });
            }
        }

        #endregion

        #region READ
        //Populate Data Utama
        public JsonResult GetVendorAkunByVendID()
        {
            return Json(mCompanyProfile.GetVendorAkunByVendID(ConnectionString, CurrLoginUser.VendID));
        }

        //Populate License for Legal Tab
        public JsonResult GetLicenseByVendID()
        {
            return Json(mCompanyProfile.GetLicenseByVendID(ConnectionString, CurrLoginUser.VendID));
        }

        //Populate Sertifikat for Legal Tab
        public JsonResult GetSertifikatByVendD(string VendID)
        {
            return Json(mCompanyProfile.GetSertifikatByVendID(ConnectionString, VendID));
        }

        #region Populate Informasi Bank for Keuangan Page
        public JsonResult GetKeuanganIDByVendID(string VendID)
        {
            return Json(mCompanyProfile.GetKeuanganIDByVendID(ConnectionString, VendID));
        }

        public JsonResult GetAllBankByKeuanganID(string KeuanganID)
        {
            return Json(mCompanyProfile.GetAllBankByKeuanganID(ConnectionString, KeuanganID));
        }
        #endregion

        //Populate Pengurus for Pengurus Tab 
        public JsonResult GetAllPengurus(string VendID)
        {
            return Json(mCompanyProfile.GetAllPengurus(ConnectionString, VendID));
        }

        //Populate Klasifikasi Bidang Usaha (Good/Service) & Pelanggan Terbesar
        public JsonResult GetProdUnggulByVendID(string VendID)
        {
            return Json(mCompanyProfile.GetProdUnggulByVendID(ConnectionString, VendID));
        }

        //Populate Notes for Notes(CATATAN) Page
        public JsonResult GetAllNotesByVendID(string VendID)
        {
            return Json(mCompanyProfile.GetAllNotesByVendID(ConnectionString, VendID));
        }
        #endregion

        #region UPDATE

        #endregion
    }
}
