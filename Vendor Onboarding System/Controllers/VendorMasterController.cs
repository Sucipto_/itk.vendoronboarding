﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Newtonsoft.Json;
using Vendor_Onboarding_System.Localize;
using VOS.Models;
using VOS.Services;

namespace Vendor_Onboarding_System.Controllers
{
    public class VendorMasterController : Controller
    {
        #region Session
        private string ConnectionString
        {
            get
            {
                try
                {
                    return JsonConvert.DeserializeObject<string>(HttpContext.Session.GetString("ConnectionString"));
                }
                catch
                {
                    return null;
                }
            }
        }

        private dbVendorAkun_Model CurrLoginUser
        {
            get
            {
                try
                {
                    return JsonConvert.DeserializeObject<dbVendorAkun_Model>(HttpContext.Session.GetString("CurrLoginUser"));
                }
                catch
                {
                    return null;
                }
            }
        }
        #endregion

        private readonly IStringLocalizer<Resource> _stringLocalizer;
        private readonly ILogger<HomeController> _logger;
        private readonly IConfiguration _config;

        CompanyProfileService mCompanyProfileService = new CompanyProfileService();

        public VendorMasterController(ILogger<HomeController> logger, IConfiguration config, IStringLocalizer<Resource> stringLocalizer)
        {
            _logger = logger;
            _config = config;
            _stringLocalizer = stringLocalizer;
        }

        public IActionResult Index()
        {
            ViewBag.Local = _stringLocalizer;

            if (CurrLoginUser == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                ViewBag.Login = true;
                ViewBag.Category = CurrLoginUser.Category;
            }

            ViewBag.Principles = mCompanyProfileService.CountGeneral(ConnectionString, "Principles");
            ViewBag.Distributor = mCompanyProfileService.CountGeneral(ConnectionString, "Distributor");
            ViewBag.SystemIntegrator = mCompanyProfileService.CountGeneral(ConnectionString, "System Integrator");
            ViewBag.LocalVendor = mCompanyProfileService.CountGeneral(ConnectionString, "Local Vendor");

            return View();
        }

        public JsonResult GetVendorMaster()
        {
            return Json(mCompanyProfileService.GetVendorMaster(ConnectionString));
        }

        public JsonResult GetAllPrinciples()
        {
            return Json(mCompanyProfileService.GetAllPrinciples(ConnectionString));
        }

        public JsonResult GetAllDistributor()
        {
            return Json(mCompanyProfileService.GetAllDistributor(ConnectionString));
        }
        public JsonResult GetAllSystemIntegrator()
        {
            return Json(mCompanyProfileService.GetAllSystemIntegrator(ConnectionString));
        }

        public JsonResult GetAllLocalVendor()
        {
            return Json(mCompanyProfileService.GetAllLocalVendor(ConnectionString));
        }
    }
}
