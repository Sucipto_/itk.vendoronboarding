﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Microsoft.SharePoint.Client;
using Newtonsoft.Json;
using System.Net.Http.Headers;
using System.Security;
using Vendor_Onboarding_System.Localize;
using VOS.Models;
using VOS.Services;

namespace Vendor_Onboarding_System.Controllers
{
    public class BillingPortalController : Controller
    {
        #region Session
        private string ConnectionString
        {
            get
            {
                try
                {
                    return JsonConvert.DeserializeObject<string>(HttpContext.Session.GetString("ConnectionString"));
                }
                catch
                {
                    return null;
                }
            }
        }

        private dbVendorAkun_Model CurrLoginUser
        {
            get
            {
                try
                {
                    return JsonConvert.DeserializeObject<dbVendorAkun_Model>(HttpContext.Session.GetString("CurrLoginUser"));
                }
                catch
                {
                    return null;
                }
            }
        }

        private dbBilling_Model LastAdd
        {
            get
            {
                try
                {
                    return JsonConvert.DeserializeObject<dbBilling_Model>(HttpContext.Session.GetString("LastAdd"));
                }
                catch
                {
                    return null;
                }
            }
        }
        #endregion

        private readonly IStringLocalizer<Resource> _stringLocalizer;
        private readonly ILogger<HomeController> _logger;
        private readonly IConfiguration _config;
        private readonly IWebHostEnvironment _hostEnvironment;

        CountryDetailService mCountryDetail = new CountryDetailService();
        BillingPortalService mBillingPortal = new BillingPortalService();

        public BillingPortalController(ILogger<HomeController> logger, IConfiguration config, IStringLocalizer<Resource> stringLocalizer, IWebHostEnvironment hostEnvironment)
        {
            _logger = logger;
            _config = config;
            _stringLocalizer = stringLocalizer;
            _hostEnvironment = hostEnvironment;
        }

        public IActionResult Index()
        {
            ViewBag.Local = _stringLocalizer;
            if (CurrLoginUser == null)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                ViewBag.Login = true;
                ViewBag.Category = CurrLoginUser.Category;
            }
            return View();
        }

        public JsonResult AddBilling(string TicketID, string NoInvoice, DateTime InvoiceDate, string Amount, string Currency, string PONumberITK, string AttachBilling, string BankID, string Status, DateTime StatusDate)
        {
            try
            {
                var data = new dbBilling_Model()
                {
                    VendID = CurrLoginUser.VendID,
                    TicketID = TicketID,
                    NoInvoice = NoInvoice,
                    InvoiceDate = InvoiceDate,
                    Amount = Amount,
                    Currency = Currency,
                    PONumberITK = PONumberITK,
                    AttachBilling = AttachBilling,
                    BankID = BankID,
                    Status = Status
                };
                data.BillID = mBillingPortal.AddBilling(ConnectionString, data);

                HttpContext.Session.SetString("LastAdd", JsonConvert.SerializeObject(data));

                return Json(new Response<string>()
                {
                    Status = "Success"
                });
            }
            catch (Exception ex)
            {
                return Json(new Response<string>()
                {
                    Status = "Failed",
                    Message = ex.Message
                });
            }
        }

        [HttpPost]
        public async Task<ActionResult> UploadFiles(IList<IFormFile> files)
        {
            string fileName = null;

            foreach (IFormFile source in files)
            {
                // Get original file name to get the extension from it.
                string orgFileName = ContentDispositionHeaderValue.Parse(source.ContentDisposition).FileName.Trim('"');

                // Create a new file name to avoid existing files on the server with the same names.
                fileName = source.FileName;

                string fullPath = GetFullPathOfFile(fileName);

                // Create the directory.
                Directory.CreateDirectory(Directory.GetParent(fullPath).FullName);

                // Save the file to the server.
                await using FileStream output = System.IO.File.Create(fullPath);
                await source.CopyToAsync(output);
            }

            try
            {
                Uri site = new Uri("https://ptintikomberlianmustika.sharepoint.com/sites/SASDeveloperTeam");
                string user = "mbahariawan@intikom.co.id";
                SecureString password = new SecureString();
                foreach (char c in "Intikom2018") password.AppendChar(c);

                // Note: The PnP Sites Core AuthenticationManager class also supports this
                using (var authenticationManager = new AuthenticationManager())
                using (var context = authenticationManager.GetContext(site, user, password))
                {
                    context.Load(context.Web, p => p.Title);
                    await context.ExecuteQueryAsync();
                    Console.WriteLine($"Title: {context.Web.Title}");

                    //coba upload file
                    Web web = context.Web;
                    context.Load(web);
                    context.ExecuteQuery();
                    FileCreationInformation newFile = new FileCreationInformation();
                    newFile.Content = System.IO.File.ReadAllBytes(GetFullPathOfFile(fileName));
                    newFile.Url = fileName;
                    List byTitle = context.Web.Lists.GetByTitle("VOB");
                    Folder folder = byTitle.RootFolder.Folders.GetByUrl("Billing");
                    context.Load(folder);
                    context.ExecuteQuery();
                    Microsoft.SharePoint.Client.File uploadFile = folder.Files.Add(newFile);
                    try
                    {
                        uploadFile.CheckIn("checkin", CheckinType.MajorCheckIn);
                        context.Load(byTitle);
                        context.Load(uploadFile);
                        context.ExecuteQuery();
                    }
                    catch
                    {

                    }
                }
            }
            catch (Exception ex)
            {
                return Ok(new Response<string>()
                {
                    Status = "Failed",
                    Message = ex.Message
                });
            }

            System.IO.File.Delete(GetFullPathOfFile(fileName));
            var data = LastAdd;
            data.AttachBilling = fileName;
            mBillingPortal.UpdateAttachBilling(ConnectionString, data);

            HttpContext.Session.Remove("LastAdd");

            return Ok(new Response<string>()
            {
                Status = "Success"
            });
        }

        public static async Task FixDownload(string fileName)
        {
            try
            {
                Uri site = new Uri("https://ptintikomberlianmustika.sharepoint.com/sites/SASDeveloperTeam");
                string user = "mbahariawan@intikom.co.id";
                SecureString password = new SecureString();
                foreach (char c in "Intikom2018") password.AppendChar(c);

                // Note: The PnP Sites Core AuthenticationManager class also supports this
                var accessToken = new AuthenticationManager().GetAccessToken(site, user, password);
                using (System.Net.WebClient client = new System.Net.WebClient())
                {
                    client.Headers.Add("X-FORMS_BASED_AUTH_ACCEPTED", "f");
                    client.Headers.Add("Authorization", "Bearer " + accessToken);
                    //string webUrl = "https://ptintikomberlianmustika.sharepoint.com/sites/SASDeveloperTeam/";
                    string webUrl = "https://ptintikomberlianmustika.sharepoint.com/sites/SASDeveloperTeam";
                    //string documentLibName = "Shared%20Documents/test";
                    string documentLibName = "VOB/Billing";
                    //string fileName = "FDD_Vob_LogIn_Reg_002.docx";

                    //link download nya
                    Uri endpointUri = new Uri(webUrl + "/_api/web/GetFolderByServerRelativeUrl('" + documentLibName + "')/Files('" + fileName + "')/$value");

                    //https://ptintikomberlianmustika.sharepoint.com/sites/SASDeveloperTeam/_api/web/GetFolderByServerRelativeUrl('VOB/Billing')/Files('makan.jpg')/$value

                    byte[] filedata = client.DownloadData(endpointUri);
                    FileStream outputStream = new FileStream(@"E:\Temp\" + fileName, FileMode.OpenOrCreate | FileMode.Append, FileAccess.Write, FileShare.None);
                    outputStream.Write(filedata, 0, filedata.Length);
                    outputStream.Flush(true);
                    outputStream.Close();
                }
            }
            catch (Exception ex)
            {
                var text = ex.Message;
            }
        }

        private string GetFullPathOfFile(string fileName)
        {
            return $"{_hostEnvironment.WebRootPath}\\uploads\\filebilling\\{fileName}";
        }

        public JsonResult GetTiketNumber()
        {
            var nextAutoIncrement = mBillingPortal.GetIDBillingPlusOne(ConnectionString).ToString();

            var angkaNol = "0000";

            var nextAutoIncrement2 = nextAutoIncrement.Length;

            string tiketNumber = "IB" + DateTime.Now.Year + angkaNol.Substring(0, angkaNol.Length - nextAutoIncrement2) + nextAutoIncrement;

            return Json(tiketNumber);
        }

        public JsonResult GetAllNamaBank()
        {
            try
            {
                var KeuanganIDbyVendID = mBillingPortal.GetAllKeuanganIDbyVendID(ConnectionString, CurrLoginUser.VendID);

                List<dbBank_Model> bank = new List<dbBank_Model>();
                foreach (var a in KeuanganIDbyVendID)
                {
                    bank.AddRange(mBillingPortal.GetDistinctBankNamebyKeuanganID(ConnectionString, a.KeuanganID));
                }

                return Json(new Response<List<dbBank_Model>>
                {
                    Status = "Success",
                    Value = bank
                });
            }
            catch (Exception ex)
            {
                return Json(new Response<string>
                {
                    Status = "Failed",
                    Message = ex.Message
                });
            }
        }

        public JsonResult GetAllCabangBank(string NamaBank)
        {
            try
            {
                var KeuanganIDbyVendID = mBillingPortal.GetAllKeuanganIDbyVendID(ConnectionString, CurrLoginUser.VendID);

                List<dbBank_Model> cabang = new List<dbBank_Model>();

                foreach (var a in KeuanganIDbyVendID)
                {
                    cabang.AddRange(mBillingPortal.GetDistinctCabangbyKeuanganID(ConnectionString, a.KeuanganID, NamaBank));
                }

                return Json(new Response<List<dbBank_Model>>
                {
                    Status = "Success",
                    Value = cabang
                });
            }
            catch (Exception ex)
            {
                return Json(new Response<string>
                {
                    Status = "Failed",
                    Message = ex.Message
                });
            }
        }

        public JsonResult GetAllNoRekening(string NamaBank, string CabangBank)
        {
            try
            {
                var KeuanganIDbyVendID = mBillingPortal.GetAllKeuanganIDbyVendID(ConnectionString, CurrLoginUser.VendID);
                List<dbBank_Model> norek = new List<dbBank_Model>();
                foreach (var a in KeuanganIDbyVendID)
                {
                    norek.AddRange(mBillingPortal.GetNoRekeningbyKNC(ConnectionString, a.KeuanganID, NamaBank, CabangBank));
                }
                return Json(new Response<List<dbBank_Model>>
                {
                    Status = "Success",
                    Value = norek
                });
            }
            catch (Exception ex)
            {
                return Json(new Response<string>
                {
                    Status = "Failed",
                    Message = ex.Message
                });
            }
        }

        public JsonResult GetBankIDbyNCR(string NamaBank, string CabangBank, string NoRekening)
        {
            try
            {
                return Json(new Response<string>
                {
                    Status = "Success",
                    Value = mBillingPortal.GetBankIDbyNCR(ConnectionString, NamaBank, CabangBank, NoRekening)
                });
            }
            catch (Exception ex)
            {
                return Json(new Response<string>
                {
                    Status = "Failed",
                    Message = ex.Message
                });
            }
        }

        public JsonResult GetBankInformationbyBankID(string BankID)
        {
            return Json(new Response<List<dbBank_Model>>
            {
                Status = "Success",
                Value = mBillingPortal.GetBankInformationbyBankID(ConnectionString, BankID)
            });
        }

        public JsonResult GetAllBilling()
        {
            return Json(mBillingPortal.GetAllBillingPortal(ConnectionString, CurrLoginUser.VendID));
        }

        public JsonResult GetAllCurrency()
        {
            return Json(new Response<List<dbCountry_Model>>
            {
                Status = "Success",
                Value = mCountryDetail.GetAllCurrency(ConnectionString)
            });
        }
    }
}
